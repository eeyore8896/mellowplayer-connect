# MellowPlayer.Connect


[![pipeline status](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/badges/master/pipeline.svg)](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/commits/master)
[![coverage report](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/badges/master/coverage.svg)](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/commits/master)

A remote control application for [MellowPlayer](https://gitlab.com/ColinDuquesnoy/MellowPlayer)

## Description

`` MellowPlayer.Connect`` is a web server application that display a simple web interface that let you remote control MellowPlayer (toggle play/pause, skip to next/previous song,...)

## Usage

- Open a terminal, navigate to the extracted archive and start ``MellowPlayer.Connect``

    ```bash
    $ ./MellowPlayer.Connect
    ```

- Open a web browser on any device in your LAN and go to ``http://YOUR_IP_ADDRESS:5000`` *(e.g: ``http://192.168.0.109:5000``)*.

##### Command Line Arguments

- ``--mellowplayer``: specify the MellowPlayer process
- ``--mellowplayer-args``: specify the MellowPlayer process command line arguments

## Hacking

- Install [.NET Core](https://dotnet.microsoft.com/download) 3.1 (including ASP.Net Core)
- Install [Nuke](https://www.nuke.build/index.html) build system 

    ```bash
    dotnet tool install Nuke.GlobalTool --global
    ``` 

- Execute nuke *(default target will compile & publish a self-contained application in a folder named ``publish``)* 

    ```bash
    nuke
    # see available commands
    nuke --help
    ```

- Now you can start the application

    ```bash
    # run app in background
    nuke start 
    # stop background app:
    nuke stop
    # or run app in current shell and use Ctrl+C to exit
    nuke run
    ```

### Screenshots

#### Mobile

![](docs/screenshots/mobile.jpg)

#### Desktop

![](docs/screenshots/desktop.png)
