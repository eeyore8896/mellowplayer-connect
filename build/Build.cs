using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Nuke.Common;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.EnvironmentInfo;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

[CheckBuildProjectConfigurations]
[UnsetVisualStudioEnvironmentVariables]
class Build : NukeBuild
{
    /// Support plugins are available for:
    ///   - JetBrains ReSharper        https://nuke.build/resharper
    ///   - JetBrains Rider            https://nuke.build/rider
    ///   - Microsoft VisualStudio     https://nuke.build/visualstudio
    ///   - Microsoft VSCode           https://nuke.build/vscode
    public static int Main() => Execute<Build>(x => x.Publish);

    [Solution] public readonly Solution Solution;

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    public readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [Parameter("Runtime to publish - Default is 'linux-x64' or 'win-x64' (server) depending on the host platform.")]
    public readonly string Runtime = RuntimeInformation.IsOSPlatform(OSPlatform.Linux) ? "linux-x64" : "win-x64";

    [Parameter("The build number component of the version - Default is 0 for local build or $CI_PIPELINE_IID for ci builds")]
    public readonly string BuildNumber = IsLocalBuild ? "0" : Environment.GetEnvironmentVariable("CI_PIPELINE_IID");

    string Version => $"0.2.1.{BuildNumber}";

    AbsolutePath SourceDirectory => RootDirectory / "src";
    AbsolutePath ArtifactsDirectory => RootDirectory / "publish" / Runtime;
    AbsolutePath AppProject => SourceDirectory / "app" / "MellowPlayer.Connect.App.csproj";
    string ExecutableExtension => RuntimeInformation.IsOSPlatform(OSPlatform.Linux) ? "" : ".exe";
    AbsolutePath Executable => ArtifactsDirectory / $"MellowPlayer.Connect{ExecutableExtension}";

    Target Clean => _ => _
        .Description("Cleans build & publish directories")
        .Before(Restore)
        .Executes(() =>
        {
            SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            EnsureCleanDirectory(ArtifactsDirectory);
        });

    Target Restore => _ => _
        .Description("Restore dependencies and tools")
        .Executes(() =>
        {
            DotNetRestore(_ => _
                .SetProjectFile(Solution)
                .SetRuntime(Runtime));
        });

    Target Compile => _ => _
        .Description("Compiles the application")
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(_ => _
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .SetProperties(new Dictionary<string, object>()
                {
                    {"Version", Version},
                })
                .SetRuntime(Runtime)
                .EnableNoRestore());
        });

    Target Test => _ => _
        .Description("Executes the test suite and generates a code coverage report")
        .DependsOn(Compile)
        .Executes(() =>
        {
            DotNetTest(_ => _
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .SetRuntime(Runtime)
                .SetProperties(new Dictionary<string, object>()
                {
                    {"CollectCoverage", true},
                    {"CoverletOutputFormat", "opencover"},
                    {"Exclude", "\"[*]*Fakes*\""}
                })
                .EnableNoRestore());
        });

    Target Publish => _ => _
        .DependsOn(Compile)
        .Description("Publishes a self contained version of the application")
        .Executes(() =>
        {
            DotNetPublish(_ => _
                .SetProject(AppProject)
                .SetSelfContained(true)
                .SetRuntime(Runtime)
                .SetProperties(new Dictionary<string, object>()
                {
                    {"Version", Version},
                })
                .SetOutput(ArtifactsDirectory)
            );

            File.WriteAllText(ArtifactsDirectory / "version", Version);
        });

    Target Stop => _ => _
        .Description("Stops the application")
        .Executes(() =>
        {
            foreach (var process in Process.GetProcessesByName("MellowPlayer.Connect"))
            {
                process.Kill();
            }
        });

    Target Start => _ => _
        .Description("Run the published application in background")
        .DependsOn(Publish)
        .DependsOn(Stop)
        .Executes(() =>
        {
            ProcessTasks.StartProcess(Executable, workingDirectory: ArtifactsDirectory, timeout: Timeout.Infinite);
            Logger.Info("Listening on: http://0.0.0.0:5000");
        });

    Target Run => _ => _
        .Description("Run the published application")
        .DependsOn(Publish)
        .DependsOn(Stop)
        .Executes(() => { ProcessTasks.StartProcess(Executable, workingDirectory: ArtifactsDirectory, timeout: Timeout.Infinite).WaitForExit(); });
}