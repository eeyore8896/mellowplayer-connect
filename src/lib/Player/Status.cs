using System;
using MellowPlayer.Connect.Lib.LocalApplication;
using MellowPlayer.Connect.Lib.Song;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace MellowPlayer.Connect.Lib.Player
{
    public class Status : IStatus
    {
        private readonly ILocalProcess _localProcess;
        private readonly ISongFactory _songFactory;
        private readonly ILogger<Status> _logger;
        private ISong _currentSong;
        private bool _isPlaying;
        private string _service;

        public ISong CurrentSong
        {
            get => _currentSong;

            set
            {
                if (!_localProcess.IsRunning)
                    value = new NullSong();

                if (_currentSong != null && _currentSong.Equals(value))
                    return;
                
                _currentSong = value;
                CurrentSongChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public bool IsPlaying
        {
            get => _isPlaying;

            set
            {
                if (value && !_localProcess.IsRunning)
                    value = false;

                if (value == _isPlaying) return;

                _isPlaying = value;
                PlayingChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public string Service
        {
            get => _service;

            set
            {
                if (value != "" && !_localProcess.IsRunning)
                    value = "";

                if (value == _service) return;
                
                _service = value;
                ServiceChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler CurrentSongChanged;
        public event EventHandler PlayingChanged;
        public event EventHandler ServiceChanged;

        public Status(ILocalProcess localProcess, ISongFactory songFactory, ILogger<Status> logger)
        {
            _localProcess = localProcess;
            _songFactory = songFactory;
            _logger = logger;

            _currentSong = new NullSong();
            _isPlaying = false;
            _service = "";
        }

        public Status(ILocalProcess localProcess, ISongFactory songFactory, IStatus otherStatus)
        {
            _localProcess = localProcess;
            _songFactory = songFactory;

            _currentSong = _songFactory.Create(otherStatus.CurrentSong.Title, otherStatus.CurrentSong.Album, otherStatus.CurrentSong.Artist,
                otherStatus.CurrentSong.ArtUrl, otherStatus.CurrentSong.IsFavorite);
            _isPlaying = otherStatus.IsPlaying;
            _service = otherStatus.Service;
        }

        public void Update(IStatus other)
        {
            IsPlaying = other.IsPlaying;
            CurrentSong = other.CurrentSong;
            Service = other.Service;
        }

        public bool Equals(IStatus other)
        {
            return CurrentSong.Equals(other.CurrentSong) && IsPlaying == other.IsPlaying && Service == other.Service;
        }

        public void Deserialize(string jsonString)
        {
            var jsonObject = JObject.Parse(jsonString);
            DeserializePlaying(jsonObject);
            DeserializeService(jsonObject);
            DeserializeCurrentSong(jsonObject);
        }

        private void DeserializePlaying(JObject jsonObject)
        {
            IsPlaying = jsonObject["isPlaying"].ToObject<bool>();
        }

        private void DeserializeService(JObject jsonObject)
        {
            Service = jsonObject["serviceName"].ToObject<string>();
        }

        private void DeserializeCurrentSong(JObject jsonObject)
        {
            var title = jsonObject["currentSong"]["title"].ToObject<string>();
            var album = jsonObject["currentSong"]["album"].ToObject<string>();
            var artist = jsonObject["currentSong"]["artist"].ToObject<string>();
            var artUrl = jsonObject["currentSong"]["artUrl"].ToObject<string>();
            var isFavorite = jsonObject["currentSong"]["isFavorite"].ToObject<bool>();

            CurrentSong = _songFactory.Create(title, album, artist, artUrl, isFavorite);
        }
    }
}