using System;
using MellowPlayer.Connect.Lib.Song;

namespace MellowPlayer.Connect.Lib.Player
{
    public interface IStatus
    {
        ISong CurrentSong { get; set; }
        bool IsPlaying { get; set;  }
        string Service { get; set; }
        
        event EventHandler CurrentSongChanged;
        event EventHandler PlayingChanged;
        event EventHandler ServiceChanged;

        void Update(IStatus other);
        bool Equals(IStatus other);

        void Deserialize(string jsonString);
    }
}