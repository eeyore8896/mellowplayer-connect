using System;
using MellowPlayer.Connect.Lib.LocalApplication;
using Microsoft.Extensions.Logging;
using Action = MellowPlayer.Connect.Lib.LocalApplication.Action;

namespace MellowPlayer.Connect.Lib.Player
{
    public class Player : IPlayer
    {
        private readonly IInteractionMethod _interactionMethod;
        private readonly ILogger<Player> _logger;

        public IStatus Status { get; }

        public Player(IInteractionMethod interactionMethod, IStatusFactory statusFactory, ILogger<Player> logger)
        {
            _interactionMethod = interactionMethod;
            _logger = logger;
            Status = statusFactory.Create();
            Status.CurrentSongChanged += (sender, args) => _logger.LogInformation($"Current song changed: {Status.CurrentSong.ToString()}");
            Status.ServiceChanged += (sender, args) => _logger.LogInformation($"Service changed: {Status.Service}");
            Status.PlayingChanged += (sender, args) => _logger.LogInformation($"IsPlaying changed: {Status.IsPlaying}");
            
            _interactionMethod.PlayerStatusUpdated += OnPlayerStatusUpdated;
            OnPlayerStatusUpdated(this, EventArgs.Empty);
        }

        private void OnPlayerStatusUpdated(object sender, EventArgs e)
        {
            Status.Update(_interactionMethod.PlayerStatus);
        }

        public void PlayPause()
        {
            _logger.LogDebug($"PlayPause called");
            _interactionMethod.Execute(Action.TogglePlayPause);
        }

        public void Next()
        {
            _logger.LogDebug($"Next called");
            _interactionMethod.Execute(Action.GoToNext);
        }

        public void Previous()
        {
            _logger.LogDebug($"Previous called");
            _interactionMethod.Execute(Action.GoToPrevious);
        }
        
        public void ToggleFavorite()
        {
            _logger.LogDebug($"ToggleFavorite called");
            _interactionMethod.Execute(Action.ToggleFavorite);
        }
    }
}