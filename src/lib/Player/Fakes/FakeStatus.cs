using System;
using MellowPlayer.Connect.Lib.Song;
using MellowPlayer.Connect.Lib.Song.Fakes;
using Newtonsoft.Json.Linq;

namespace MellowPlayer.Connect.Lib.Player.Fakes
{
    public class FakeStatus : IStatus
    {
        private ISong _currentSong;
        private bool _isPlaying;
        private string _service;

        public ISong CurrentSong
        {
            get => _currentSong;
            set
            {
                if (_currentSong != null && _currentSong.Equals(value))
                    return;
                
                _currentSong = value;
                CurrentSongChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public bool IsPlaying
        {
            get => _isPlaying;
            set
            {
                if (value == _isPlaying)
                    return;

                _isPlaying = value;
                PlayingChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public string Service
        {
            get => _service;
            set
            {
                if (value == _service)
                    return;

                _service = value;
                ServiceChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public FakeStatus() : this(new FakeSong(), false, "")
        {
            
        }

        public FakeStatus(ISong song, bool isPlaying, string service)
        {
            CurrentSong = new FakeSong(song.Title, song.Album, song.Artist, song.ArtUrl, song.IsFavorite);
            IsPlaying = isPlaying;
            Service = service;
        }

        public event EventHandler CurrentSongChanged;
        public event EventHandler PlayingChanged;
        public event EventHandler ServiceChanged;
        
        public void Update(IStatus other)
        {
            CurrentSong = other.CurrentSong;
            IsPlaying = other.IsPlaying;
            Service = other.Service;
        }

        public bool Equals(IStatus other)
        {
            return CurrentSong.Equals(other.CurrentSong) && IsPlaying == other.IsPlaying && Service == other.Service;
        }

        public void SetPlaying(bool isPlaying)
        {
            IsPlaying = isPlaying;
        }

        public void SetCurrentSong(ISong song)
        {
            CurrentSong = song;
        }

        public void SetService(string service)
        {
            Service = service;
        }

        public void Deserialize(string jsonString)
        {
            var jsonObject = JObject.Parse(jsonString);
            DeserializePlaying(jsonObject);
            DeserializeService(jsonObject);
            DeserializeCurrentSong(jsonObject);
        }

        private void DeserializePlaying(JObject jsonObject)
        {
            IsPlaying = jsonObject["isPlaying"].ToObject<bool>();
        }

        private void DeserializeService(JObject jsonObject)
        {
            Service = jsonObject["serviceName"].ToObject<string>();
        }

        private void DeserializeCurrentSong(JObject jsonObject)
        {
            var title = jsonObject["currentSong"]["title"].ToObject<string>();
            var album = jsonObject["currentSong"]["album"].ToObject<string>();
            var artist = jsonObject["currentSong"]["artist"].ToObject<string>();
            var artUrl = jsonObject["currentSong"]["artUrl"].ToObject<string>();
            var isFavorite = jsonObject["currentSong"]["isFavorite"].ToObject<bool>();

            CurrentSong = new FakeSong(title, album, artist, artUrl, isFavorite);
        }
    }
}