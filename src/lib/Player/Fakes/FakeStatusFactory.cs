namespace MellowPlayer.Connect.Lib.Player.Fakes
{
    public class FakeStatusFactory : IStatusFactory
    {
        public IStatus Create()
        {
            return new FakeStatus();
        }

        public IStatus Create(IStatus other)
        {
            return new FakeStatus(other.CurrentSong, other.IsPlaying, other.Service);
        }
    }
}