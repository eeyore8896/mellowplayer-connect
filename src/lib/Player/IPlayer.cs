namespace MellowPlayer.Connect.Lib.Player
{
    public interface IPlayer
    {
        IStatus Status { get; }

        void PlayPause();
        void Next();
        void Previous();

        void ToggleFavorite();
    }
}