namespace MellowPlayer.Connect.Lib.Player
{
    public interface IStatusFactory
    {
        IStatus Create();
        IStatus Create(IStatus other);
    }
}