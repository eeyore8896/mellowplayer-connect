namespace MellowPlayer.Connect.Lib.Utils
{
    public class ProcessFactory : IProcessFactory
    {
        public IProcess Create(string fileName, string arguments)
        {
            return Create(fileName, arguments, true);
        }

        public IProcess Create(string fileName, string arguments, bool redirectOutput)
        {
            return new Process(fileName, arguments, redirectOutput);
        }
    }
}