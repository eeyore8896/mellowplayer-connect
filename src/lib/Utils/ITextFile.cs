namespace MellowPlayer.Connect.Lib.Utils
{
    public interface ITextFile
    {
        bool Exists();
        
        string Read();
    }
}