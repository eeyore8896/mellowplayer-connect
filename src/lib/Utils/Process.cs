using System;

namespace MellowPlayer.Connect.Lib.Utils
{
    public class Process : IProcess
    {
        public int ExitCode
        {
            get
            {
                try
                {
                    return _process.ExitCode;
                }
                catch (InvalidOperationException)
                {
                    return -1;
                }
            }
        }

        public string StandardOutput => _redirectOutput ? _process.StandardOutput.ReadToEnd() : "";
        public string ErrorOutput => _redirectOutput ? _process.StandardError.ReadToEnd() : "";

        private readonly string _filePath;
        private readonly string _arguments;
        private readonly bool _redirectOutput;

        private readonly System.Diagnostics.Process _process;

        public Process(string filePath, string arguments = "", bool redirectOutput = true)
        {
            _filePath = filePath;
            _arguments = arguments;
            _redirectOutput = redirectOutput;

            _process = new System.Diagnostics.Process
            {
                StartInfo =
                {
                    FileName = _filePath,
                    Arguments = _arguments,
                    RedirectStandardError = _redirectOutput,
                    RedirectStandardOutput = _redirectOutput,
                    UseShellExecute = false
                }
            };
        }

        public void Start()
        {
            _process.Start();
        }

        public void Terminate()
        {
            _process.Kill();
        }

        public void WaitForExit()
        {
            _process.WaitForExit();
        }

        public bool IsRunning()
        {
            var processes = System.Diagnostics.Process.GetProcessesByName(_filePath);
            return processes.Length > 0;
        }
    }
}