using System;
using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.Lib.Utils
{
    public class FileWatcher : IFileWatcher
    {
        public event EventHandler Changed;

        private const int TimerInterval = 100;
        
        private readonly ITimer _timer;
        private readonly ILogger<FileWatcher> _logger;
        private readonly ITextFileFactory _textFileFactory;
        private ITextFile _textFile;
        
        private string _filePath;
        private string _previousContent;

        public FileWatcher(ITextFileFactory textFileFactoryFactory, ITimer timer, ILogger<FileWatcher> logger)
        {
            _timer = timer;
            _logger = logger;
            _textFileFactory = textFileFactoryFactory;
        }

        public void Watch(string filePath)
        {
            _textFile = _textFileFactory.Create(filePath);
            _previousContent = null;
            _filePath = filePath;
            _timer.Start(TimerInterval, OnTimerElapsed);
            OnTimerElapsed();
        }

        private void OnTimerElapsed()
        {
            var content = _textFile.Read();
            if (_previousContent == content)
                return;
            
            _logger.LogTrace($"File changed: {_filePath}");
            
            _previousContent = content;
            OnChanged();
        }

        private void OnChanged()
        {
            Changed?.Invoke(this, EventArgs.Empty);
        }
    }
}