namespace MellowPlayer.Connect.Lib.Utils
{
    public interface IProcess
    {
        int ExitCode { get; }
        string StandardOutput { get; }
        string ErrorOutput { get; }

        void Start();
        void Terminate();
        void WaitForExit();

        bool IsRunning();
    }
}