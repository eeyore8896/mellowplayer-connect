namespace MellowPlayer.Connect.Lib.Utils
{
    public interface IProcessFactory
    {
        IProcess Create(string fileName, string arguments);
        IProcess Create(string fileName, string arguments, bool redirectOutput);
    }
}