using System;

namespace MellowPlayer.Connect.Lib.Utils
{
    public interface IFileWatcher
    {
        event EventHandler Changed;

        void Watch(string filePath);
    }
}