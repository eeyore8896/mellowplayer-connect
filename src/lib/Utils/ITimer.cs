using System;

namespace MellowPlayer.Connect.Lib.Utils
{
    public interface ITimer
    {
        void Stop();
        void Start(int interval, Action callback);
        void Start(int interval, bool autoReset, Action callback);
    }
}