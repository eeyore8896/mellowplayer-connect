namespace MellowPlayer.Connect.Lib.Utils
{
    public interface ITextFileFactory
    {
        ITextFile Create(string path);
    }
}