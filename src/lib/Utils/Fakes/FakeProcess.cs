using System.Collections.Generic;

namespace MellowPlayer.Connect.Lib.Utils.Fakes
{
    public class FakeProcess : IProcess
    {
        public string FileName { get; }
        public string Arguments { get; }
        
        private bool _isRunning;
        private string Key => $"{FileName}{Arguments}";
        
        public int ExitCode { get; set; }
        public string StandardOutput { get; set; }

        public string ErrorOutput { get; set; }

        public static readonly Dictionary<string, FakeProcess> RunningProcesses = new Dictionary<string, FakeProcess>();

        public FakeProcess(string fileName, string arguments, bool redirectOutput)
        {
            FileName = fileName;
            Arguments = arguments;
            _isRunning = false;
            
            ExitCode = 0;
            StandardOutput = "";
            ErrorOutput = "";
        }

        public void Start()
        {
            _isRunning = true;
            RunningProcesses[Key] = this;
        }


        public void Terminate()
        {
            _isRunning = false;

            if (RunningProcesses.ContainsKey(Key))
                RunningProcesses.Remove(Key);
        }

        public void WaitForExit()
        {
            Terminate();
        }

        public bool IsRunning()
        {
            return _isRunning;
        }
    }
}