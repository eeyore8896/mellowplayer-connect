using System;

namespace MellowPlayer.Connect.Lib.Utils.Fakes
{
    public class FakeFileWatcher : IFileWatcher
    {
        public event EventHandler Changed;
        
        public void Watch(string filePath)
        {
            
        }

        public void Trigger()
        {
            Changed?.Invoke(this, EventArgs.Empty);
        }
    }
}