namespace MellowPlayer.Connect.Lib.Utils.Fakes
{
    public class FakeProcessFactory : IProcessFactory
    {
        public FakeProcess Last { get; private set; } = null;
        
        public IProcess Create(string fileName, string arguments)
        {
            return Create(fileName, arguments, true);
        }

        public IProcess Create(string fileName, string arguments, bool redirectOutput)
        {
            Last = new FakeProcess(fileName, arguments, redirectOutput);
            return Last;
        }
        
        
    }
}