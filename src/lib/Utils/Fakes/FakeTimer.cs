using System;

namespace MellowPlayer.Connect.Lib.Utils.Fakes
{
    public class FakeTimer : ITimer
    {
        private Action _callback;

        private bool IsRunning { get; set; }

        public void Trigger()
        {
            if (IsRunning)
                _callback();
        }

        public void Stop()
        {
            IsRunning = false;
        }

        public void Start(int interval, Action callback)
        {
            Start(interval, true, callback);
        }

        public void Start(int interval, bool autoReset, Action callback)
        {
            IsRunning = true;
            _callback = callback;
        }
    }
}