namespace MellowPlayer.Connect.Lib.Utils.Fakes
{
    public class FakeTextFileFactory : ITextFileFactory
    {
        public ITextFile Create(string path)
        {
            return new FakeTextFile(path);
        }
    }
}