using System;

namespace MellowPlayer.Connect.Lib.Song
{
    public class NullSong : ISong
    {
        public string Title { get; }
        public string Album { get; }
        public string Artist { get; }
        public string ArtUrl { get; }
        public bool IsFavorite { get; private set; }
        
        public NullSong()
        {
            Title = "";
            Album = "";
            Artist = "";
            ArtUrl = "";
            IsFavorite = false;
        }
        
        public void ToggleFavorite()
        {
            
        }

        public bool Equals(ISong otherSong)
        {
            return otherSong is NullSong;
        }

        public void Update(ISong otherSong)
        {
            FavoriteChanged?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler FavoriteChanged;
    }
}