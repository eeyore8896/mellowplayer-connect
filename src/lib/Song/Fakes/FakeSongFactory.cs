namespace MellowPlayer.Connect.Lib.Song.Fakes
{
    public class FakeSongFactory : ISongFactory
    {
        public ISong Create(string title, string album, string artist, string artUrl, bool isFavorite)
        {
            return new FakeSong(title, album, artist, artUrl, isFavorite);
        }
    }
}