using System;

namespace MellowPlayer.Connect.Lib.Song.Fakes
{
    public class FakeSong : ISong
    {
        public string Title { get; private set; }
        public string Album { get; private set; }
        public string Artist { get; private set; }
        public string ArtUrl { get; private set; }
        public bool IsFavorite { get; private set; }

        public FakeSong() : this(
            "",
            "",
            "",
            "",
            false)
        {
            
        }

        public FakeSong(bool generateGuidForProperties): this(
            Guid.NewGuid().ToString(),
            Guid.NewGuid().ToString(),
            Guid.NewGuid().ToString(),
            Guid.NewGuid().ToString(),
            false)
        {
            
        }

        public FakeSong(string title, string album, string artist, string artUrl, bool isFavorite)
        {
            Title = title;
            Album = album;
            Artist = artist;
            ArtUrl = artUrl;
            IsFavorite = isFavorite;
        }

        public void ToggleFavorite()
        {
            IsFavorite = !IsFavorite;
            OnFavoriteChanged();
        }

        public bool Equals(ISong otherSong)
        {
            return Title == otherSong.Title &&
                   Album == otherSong.Album &&
                   Artist == otherSong.Artist &&
                   ArtUrl == otherSong.ArtUrl &&
                   IsFavorite == otherSong.IsFavorite;
        }
        
        private void OnFavoriteChanged()
        {
            FavoriteChanged?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler FavoriteChanged;
    }
}