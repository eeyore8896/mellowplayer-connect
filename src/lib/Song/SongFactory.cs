using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.Lib.Song
{
    public class SongFactory : ISongFactory
    {
        private readonly ILogger<Connect.Lib.Song.Song> _logger;

        public SongFactory(ILogger<Connect.Lib.Song.Song> logger)
        {
            _logger = logger;
        }

        public ISong Create(string title, string album, string artist, string artUrl, bool isFavorite)
        {
            return new Connect.Lib.Song.Song(_logger, title, album, artist, artUrl, isFavorite);
        }
    }
}