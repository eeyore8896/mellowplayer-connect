using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.Lib.Song
{
    public class Song : ISong
    {
        private readonly ILogger<Song> _logger;

        public Song(ILogger<Song> logger, string title, string album, string artist, string artUrl, bool isFavorite)
        {
            _logger = logger;

            Title = title;
            Album = album;
            Artist = artist;
            ArtUrl = artUrl;
            IsFavorite = isFavorite;
        }

        public string Title { get; }
        public string Album { get; }
        public string Artist { get; }
        public string ArtUrl { get; }
        public bool IsFavorite { get; }
        
        public bool Equals(ISong otherSong)
        {
            return Title == otherSong.Title &&
                   Album == otherSong.Album &&
                   Artist == otherSong.Artist &&
                   ArtUrl == otherSong.ArtUrl &&
                   IsFavorite == otherSong.IsFavorite;
        }
        
        public override string ToString()
        {
            return $"Song(title={Title}, album={Album}, artist={Artist}, isFavorite={IsFavorite.ToString()})";
        }
    }
}