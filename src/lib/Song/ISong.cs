namespace MellowPlayer.Connect.Lib.Song
{
    public interface ISong
    {
        string Title { get; }
        string Album { get; }
        string Artist { get; }
        string ArtUrl { get; }
        bool IsFavorite { get; }

        bool Equals(ISong otherSong);
    }
}