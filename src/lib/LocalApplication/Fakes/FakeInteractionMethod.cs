using System;
using MellowPlayer.Connect.Lib.Player;
using MellowPlayer.Connect.Lib.Player.Fakes;
using MellowPlayer.Connect.Lib.Song;
using MellowPlayer.Connect.Lib.Song.Fakes;
using Action = MellowPlayer.Connect.Lib.LocalApplication.Action;

namespace MellowPlayer.Connect.Lib.LocalApplication.Fakes
{
    public class FakeInteractionMethod : IInteractionMethod
    {
        private string _version = "3.6.0";
        
        private FakeStatus _playerStatus = new FakeStatus();

        public IStatus PlayerStatus
        {
            get => _playerStatus;
            private set
            {
                if (!(value is FakeStatus fakeStatus)) return;
                
                if (_playerStatus.Equals(fakeStatus))
                    return;

                _playerStatus = fakeStatus;
                OnPlayerStatusUpdated();
            }
        }

        private void OnPlayerStatusUpdated()
        {
            PlayerStatusUpdated?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler PlayerStatusUpdated;

        public string Execute(Action action)
        {
            switch (action)
            {
                case Action.ToggleFavorite:
                    SetCurrentSong(new FakeSong(
                        _playerStatus.CurrentSong.Title, 
                        _playerStatus.CurrentSong.Album,
                        _playerStatus.CurrentSong.Artist,
                        _playerStatus.CurrentSong.ArtUrl,
                        !_playerStatus.CurrentSong.IsFavorite));
                    break;
                case Action.TogglePlayPause:
                    _playerStatus.SetPlaying(!PlayerStatus.IsPlaying);
                    OnPlayerStatusUpdated();
                    break;
                case Action.GoToNext:
                    SetCurrentSong(new FakeSong(true));
                    break;
                case Action.GoToPrevious:
                    SetCurrentSong(new FakeSong(true));
                    break;
                case Action.PrintVersion:
                    return _version;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }

            return "";
        }

        public void SetVersion(string version)
        {
            _version = version;
        }

        public void SetCurrentSong(ISong song)
        {
            _playerStatus.SetCurrentSong(song);
            OnPlayerStatusUpdated();
        }
    }
}