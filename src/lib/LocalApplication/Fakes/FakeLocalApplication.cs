using System;

namespace MellowPlayer.Connect.Lib.LocalApplication.Fakes
{
    public class FakeLocalApplication : ILocalApplication
    {
        private bool _isRunning;

        public bool IsRunning
        {
            get => _isRunning;
            
            private set
            {
                if (value == _isRunning) return;
                
                _isRunning = value;
                RunningChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public bool IsAvailable { get; set; }
        public string Version { get; set; }

        public bool CanToggleFavorite => true;

        public event EventHandler RunningChanged;
        
        public void Start()
        {
            SetRunning(true);
        }

        public void SetRunning(bool isRunning)
        {
            IsRunning = isRunning;
        }
    }
}