using System;
using System.Collections.Generic;
using MellowPlayer.Connect.Lib.Player;
using Action = MellowPlayer.Connect.Lib.LocalApplication.Action;

namespace MellowPlayer.Connect.Lib.LocalApplication.Fakes
{
    public class FakeLocalProcessInteractionMethod : ILocalProcessInteractionMethod
    {
        public IStatus PlayerStatus { get; set; }
        
        #pragma warning disable 67    
        public event EventHandler PlayerStatusUpdated;
        
        public List<Action> ExecutedActions { get; } = new List<Action>();

        public string Execute(Action action)
        {
            ExecutedActions.Add(action);
            return "";
        }
    }
}