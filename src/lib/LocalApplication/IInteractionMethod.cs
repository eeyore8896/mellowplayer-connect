using System;
using MellowPlayer.Connect.Lib.Player;

namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public interface IInteractionMethod
    {
        IStatus PlayerStatus { get; }
        
        event EventHandler PlayerStatusUpdated;
        
        string Execute(Action action);
    }
}