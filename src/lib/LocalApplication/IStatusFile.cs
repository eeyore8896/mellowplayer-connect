using System;
using MellowPlayer.Connect.Lib.Player;

namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public interface IStatusFile
    {
        IStatus Value { get; }

        event EventHandler Updated;
    }
}