using System;
using MellowPlayer.Connect.Lib.Player;

namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public interface ILocalProcessInteractionMethod : IInteractionMethod
    {
        
    }
    
    public class LocalProcessInteractionMethod : ILocalProcessInteractionMethod
    {
        private readonly IStatusFile _statusFile;
        private readonly ILocalProcess _localProcess;
        public IStatus PlayerStatus => _statusFile.Value;
        
        public event EventHandler PlayerStatusUpdated;

        public LocalProcessInteractionMethod(IStatusFile statusFile, ILocalProcess localProcess)
        {
            _statusFile = statusFile;
            _localProcess = localProcess;
            _statusFile.Updated += (sender, args) => PlayerStatusUpdated?.Invoke(this, EventArgs.Empty);
        }

        public string Execute(Action action)
        {
            return _localProcess.Execute(action);
        }
    }
}