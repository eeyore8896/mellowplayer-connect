namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public interface ILocalProcess
    {
        string Path { get; }
        string Arguments { get; }
        bool Exists { get; }
        bool IsRunning { get;  }

        void Start();
        string Execute(Action action);
    }
}