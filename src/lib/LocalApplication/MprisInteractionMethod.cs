using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MellowPlayer.Connect.Lib.DBus;
using MellowPlayer.Connect.Lib.Player;
using MellowPlayer.Connect.Lib.Song;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using Tmds.DBus;
using IPlayer = MellowPlayer.Connect.Lib.DBus.IPlayer;

namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public class MprisInteractionMethod : IInteractionMethod
    {
        private const string MediaPlayerService = "org.mpris.MediaPlayer2.MellowPlayer3";
        private static readonly ObjectPath MediaPlayerPath = new ObjectPath("/org/mpris/MediaPlayer2");

        private readonly ILocalProcessInteractionMethod _localProcessInteractionMethod;
        private readonly IStatusFactory _statusFactory;
        private readonly ISongFactory _songFactory;
        private readonly IConnectionFactory _connectionFactory;

        private IPlayer _player;
        private IConnection _connection;
        private readonly IStatus _status;

        public IStatus PlayerStatus
        {
            get => _statusFactory.Create(_status);
            private set => _status.Update(value);
        }

        public event EventHandler PlayerStatusUpdated;

        public MprisInteractionMethod(IStatusFactory statusFactory, 
                                      ISongFactory songFactory, 
                                      IConnectionFactory connectionFactory,
                                      ILocalProcessInteractionMethod localProcessInteractionMethod)
        {
            _localProcessInteractionMethod = localProcessInteractionMethod;
            _statusFactory = statusFactory;
            _songFactory = songFactory;
            _connectionFactory = connectionFactory;

            _status = statusFactory.Create();
            _status.CurrentSongChanged += (sender, args) => PlayerStatusUpdated?.Invoke(this, EventArgs.Empty);
            _status.PlayingChanged += (sender, args) => PlayerStatusUpdated?.Invoke(this, EventArgs.Empty);

            Connect();
        }

        private void Connect()
        {
            Task.Run(async () =>
            {
                _connection = _connectionFactory.Create();
                await _connection.ConnectAsync();
                _player = _connection.CreateProxy<IPlayer>(MediaPlayerService, MediaPlayerPath);
                await _player.WatchPropertiesAsync(OnPropertiesChanged);
                try
                {
                    await Initialize();
                }
                catch (DBusException)
                {
                    // Not a problem, OnPropertiesChanged will be called as soon as the process starts
                }
            }).Wait();
        }

        private async Task Initialize()
        {
            _status.CurrentSong = SongFromMetadata(await _player.GetAsync<IDictionary<string, object>>(nameof(PlayerProperties.Metadata)));
            _status.IsPlaying = IsPlaying(await _player.GetAsync<string>(nameof(PlayerProperties.PlaybackStatus)));
        }

        private void OnPropertiesChanged(PropertyChanges changes)
        {
            // trick to avoid being polluted by other mpris players (e.g. vlc)
            var metadata = changes.Get<IDictionary<string, object>>(nameof(PlayerProperties.Metadata));
            if (!metadata.ContainsKey("origin"))
                return;
            string origin = metadata["origin"] as string;
            if (origin != "MellowPlayer")
                return;
            
            
            if (metadata != null)
            {
                _status.CurrentSong = SongFromMetadata(metadata);
            }

            var playbackStatus = changes.Get<string>(nameof(PlayerProperties.PlaybackStatus));
            if (playbackStatus != null)
            {
                _status.IsPlaying = IsPlaying(playbackStatus);
            }
        }

        private static bool IsPlaying(string playbackStatus)
        {
            return playbackStatus == "Playing";
        }

        private ISong SongFromMetadata(IDictionary<string, object> metadata)
        {
            return _songFactory.Create(GetTitle(metadata), GetAlbum(metadata), GetArtist(metadata), ToBase64(GetArtUrl(metadata)), IsFavorite(metadata));
        }

        private static string ToBase64(string artUrl)
        {
            try
            {
                var image = Image.Load(artUrl.Replace("file://", ""));
                using (var outputStream = new MemoryStream())
                {
                    image.Save(outputStream, new JpegEncoder());
                    var bytes = outputStream.ToArray();
                    return $"data:image/jpeg;base64,{Convert.ToBase64String(bytes)}";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        private static string GetTitle(IDictionary<string, object> metadata)
        {
            const string key = "xesam:title";
            return metadata.ContainsKey(key) ? metadata[key] as string : "";
        }

        private static string GetArtist(IDictionary<string, object> metadata)
        {
            const string key = "xesam:artist";

            return metadata.ContainsKey(key) && metadata[key] is string[] artists ? string.Join(", ", artists) : "";
        }

        private static string GetAlbum(IDictionary<string, object> metadata)
        {
            const string key = "xesam:album";
            return metadata.ContainsKey(key) ? metadata[key] as string : "";
        }

        private static string GetArtUrl(IDictionary<string, object> metadata)
        {
            const string key = "mpris:artUrl";
            return metadata.ContainsKey(key) ? metadata[key] as string : "";
        }
        
        private static bool IsMellowPlayerMetadata(IDictionary<string, object> metadata)
        {
             const string key = "mpris:trackid";
             var value = metadata.ContainsKey(key) ? metadata[key] as string : null;
             return value != null && value.Contains("MellowPlayer");
        }

        private static bool IsFavorite(IDictionary<string, object> metadata)
        {
            const string key = "xesam:userRating";
            return metadata.ContainsKey(key) && (int) metadata[key] > 0;
        }

        public string Execute(Action action)
        {
            switch (action)
            {
                case Action.TogglePlayPause:
                    Task.Run(async () => { if (_player != null) await _player.PlayPauseAsync(); }).Wait();
                    break;
                case Action.GoToNext:
                    Task.Run(async () => { if (_player != null) await _player.NextAsync(); }).Wait();
                    break;
                case Action.GoToPrevious:
                    Task.Run(async () => { if (_player != null) await _player.PreviousAsync(); }).Wait();
                    break;
                case Action.ToggleFavorite:
                case Action.PrintVersion:
                    return _localProcessInteractionMethod.Execute(action);
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
            return "";
        }
    }
}