using System;
using System.IO;
using MellowPlayer.Connect.Lib.Player;
using MellowPlayer.Connect.Lib.Utils;

namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public class StatusFile : IStatusFile
    {
        private readonly ITextFileFactory _textFileFactory;
        private readonly IStatusFactory _statusFactory;
        private readonly IStatus _value;
        
        public static string FilePath => Path.Combine(Path.GetTempPath(), "mellowplayer-status.json");

        public IStatus Value
        {
            get => _statusFactory.Create(_value);
            private set => _value.Update(value);
        }
        
        public event EventHandler Updated;

        public StatusFile(ITextFileFactory textFileFactory, IFileWatcher fileWatcher, IStatusFactory statusFactory)
        {
            _textFileFactory = textFileFactory;
            var fileWatcher1 = fileWatcher;
            _statusFactory = statusFactory;

            _value = _statusFactory.Create();
            _value.CurrentSongChanged += InvokeUpdated;
            _value.ServiceChanged += InvokeUpdated;
            _value.PlayingChanged += InvokeUpdated;

            fileWatcher1.Watch(FilePath);
            fileWatcher1.Changed += OnChanged;
            OnChanged(this, EventArgs.Empty);
        }

        private void InvokeUpdated(object sender, EventArgs e)
        {
            Updated?.Invoke(this, EventArgs.Empty);
        }

        private void OnChanged(object sender, EventArgs e)
        {
            var statusFileContent = _textFileFactory.Create(FilePath).Read();
            if (statusFileContent != "")
                Value = Deserialize(statusFileContent);
        }

        private IStatus Deserialize(string statusFileContent)
        {
            var status = _statusFactory.Create();
            status.Deserialize(statusFileContent);
            return status;
        }
    }
}