using System;
using MellowPlayer.Connect.Lib.Utils;
using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public class LocalApplication : ILocalApplication
    {
        private readonly ILocalProcess _localProcess;
        private readonly ILogger<LocalApplication> _logger;
        private string _version = null;
        private bool _isRunning;

        public bool IsRunning
        {
            get => _isRunning;
            private set
            {
                if (value == _isRunning) return;

                _isRunning = value;
                _logger.LogInformation($"Running changed: {IsRunning}");
                RunningChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public bool IsAvailable => _localProcess.Exists;
        public string Version => IsAvailable ? _version : "";

        public bool CanToggleFavorite
        {
            get
            {
                try
                {
                    return IsAvailable && new Version(Version) >= new Version("3.5.6");
                }
                catch(ArgumentException)
                {
                    return false;
                }
            }
        }

        private static string ParseVersion(string versionOutput)
        {
            foreach (var line in versionOutput.Split("\n"))
            {
                if (line.StartsWith("MellowPlayer3 "))
                    return line.Replace("MellowPlayer3 ", "");
            }
            return "";
        }

        public event EventHandler RunningChanged;

        public LocalApplication(ILocalProcess localProcess, ITimer timer, ILogger<LocalApplication> logger)
        {
            _localProcess = localProcess;
            _logger = logger;

            if (IsAvailable)
            {
                _version = ParseVersion(_localProcess.Execute(Action.PrintVersion));
                _logger.LogInformation($"Controlling MellowPlayer v{Version}");
                IsRunning = _localProcess.IsRunning;
                timer.Start(1000, () => IsRunning = _localProcess.IsRunning);
            }
            else
            {
                _version = "";
                _logger.LogWarning($"MellowPlayer process not found");
                IsRunning = false;
            }
            
            _logger.LogInformation($"Can Toggle Favorite Song: {CanToggleFavorite}");
        }

        public void Start()
        {
            if (IsRunning) return;

            _logger.LogDebug("Start called");
            _localProcess.Start();
            IsRunning = true;
        }
    }
}