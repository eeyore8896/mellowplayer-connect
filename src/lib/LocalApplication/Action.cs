namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public enum Action
    {
        ToggleFavorite,
        TogglePlayPause,
        GoToNext,
        GoToPrevious,
        PrintVersion
    }
}