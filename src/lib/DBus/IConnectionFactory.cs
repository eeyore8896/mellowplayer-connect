using Tmds.DBus;

namespace MellowPlayer.Connect.Lib.DBus
{
    public interface IConnectionFactory
    {
        IConnection Create();
    }
}