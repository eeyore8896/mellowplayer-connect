using Tmds.DBus;

namespace MellowPlayer.Connect.Lib.DBus
{
    public class DbusSessionConnectionFactory : IConnectionFactory
    {
        public IConnection Create()
        {
            return new Connection(Address.Session);
        }
    }
}