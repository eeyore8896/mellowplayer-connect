using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tmds.DBus;

namespace MellowPlayer.Connect.Lib.DBus.Fakes
{
    class FakeDisposable : IDisposable
    {
        public void Dispose()
        {
            
        }
    }
    
    public class FakePlayer : IPlayer
    {
        private Action<PropertyChanges> _propertiesChangedHandler = null;
        public ObjectPath ObjectPath => new ObjectPath("fake.mpris.Player");
        public bool PlayPauseCalled { get; private set; }
        public bool NextCalled { get; private set; }
        public bool PreviousCalled { get; private set; }
        public int InitialUserRating => 1;

        public string InitialArtUrl => "";

        public string InitialAlbum => "Album";

        public string InitialArtist => "Artist";

        public string InitialSongtitle => "SongTitle";

        public void OnPropertiesChanged(PropertyChanges propertyChanges)
        {
            _propertiesChangedHandler.Invoke(propertyChanges);
        }
        
        public Task PlayPauseAsync()
        {
            PlayPauseCalled = true;
            return Task.FromResult(true);
        }

        public Task NextAsync()
        {
            NextCalled = true;
            return Task.FromResult(true);
        }

        public Task PreviousAsync()
        {
            PreviousCalled = true;
            return Task.FromResult(true);
        }

        public Task<IDictionary<string, object>> GetAllAsync()
        {
            IDictionary<string, object> result = new Dictionary<string, object>();
            return Task.FromResult(result);
        }

        public Task<T> GetAsync<T>(string prop)
        {
            object data = null;
            
            switch (prop)
            {
                case "PlaybackStatus":
                    data = "Playing";
                    break;
                case "Metadata":
                    var metadata = new Dictionary<string, object>();
                    metadata["xesam:title"] = InitialSongtitle;
                    metadata["xesam:artist"] = new string[]{InitialArtist};
                    metadata["xesam:album"] = InitialAlbum;
                    metadata["mpris:artUrl"] = InitialArtUrl;
                    metadata["xesam:userRating"] = InitialUserRating;
                    data = metadata;
                    break;
            }

            return Task.FromResult((T) data);
        }

        public Task SetAsync(string prop, object val)
        {
            return Task.CompletedTask;
        }

        public Task<IDisposable> WatchPropertiesAsync(Action<PropertyChanges> handler)
        {
            _propertiesChangedHandler = handler;
            IDisposable result = new FakeDisposable();
            return Task.FromResult(result);
        }
    }
}