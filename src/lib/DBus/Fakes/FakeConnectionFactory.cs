using Tmds.DBus;

namespace MellowPlayer.Connect.Lib.DBus.Fakes
{
    public class FakeConnectionFactory : IConnectionFactory
    {
        public FakeConnection FakeConnection { get; set; }

        public FakeConnectionFactory()
        {
            FakeConnection = new FakeConnection();
        }

        public IConnection Create()
        {
            return FakeConnection;
        }
    }
}