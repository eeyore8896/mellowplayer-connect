using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tmds.DBus;

namespace MellowPlayer.Connect.Lib.DBus.Fakes
{
    public class FakeConnection : IConnection
    {
        public FakePlayer Player { get; }
        
        public FakeConnection()
        {
            Player = new FakePlayer();
        }
        
        public void Dispose()
        {
            
        }

        public Task<ConnectionInfo> ConnectAsync()
        {
            return Task.FromResult(OnConnected());
        }

        private static ConnectionInfo OnConnected()
        {
            return new ConnectionInfo("fakeConnection");
        }

        public T CreateProxy<T>(string serviceName, ObjectPath path)
        {
            object proxy = Player;
            return (T) proxy;
        }

        public Task<string[]> ListServicesAsync()
        {
            return Task.FromResult(new string[]{});
        }

        public Task<string[]> ListActivatableServicesAsync()
        {
            return Task.FromResult(new string[]{});
        }

        public Task<string> ResolveServiceOwnerAsync(string serviceName)
        {
            return Task.FromResult("");
        }

        public Task<IDisposable> ResolveServiceOwnerAsync(string serviceName, Action<ServiceOwnerChangedEventArgs> handler, Action<Exception> onError = null)
        {
            return Task.FromResult((IDisposable) new FakeDisposable());
        }

        public Task<ServiceStartResult> ActivateServiceAsync(string serviceName)
        {
            return Task.FromResult(new ServiceStartResult());
        }

        public Task<bool> IsServiceActiveAsync(string serviceName)
        {
            return Task.FromResult(true);
        }

        public Task QueueServiceRegistrationAsync(string serviceName, Action onAquired = null, Action onLost = null, ServiceRegistrationOptions options = ServiceRegistrationOptions.Default)
        {
            return Task.FromResult(0);
        }

        public Task QueueServiceRegistrationAsync(string serviceName, ServiceRegistrationOptions options = ServiceRegistrationOptions.Default)
        {
            return Task.FromResult(0);
        }

        public Task RegisterServiceAsync(string serviceName, Action onLost = null, ServiceRegistrationOptions options = ServiceRegistrationOptions.Default)
        {
            return Task.FromResult(0);
        }

        public Task RegisterServiceAsync(string serviceName, ServiceRegistrationOptions options)
        {
            return Task.FromResult(0);
        }

        public Task<bool> UnregisterServiceAsync(string serviceName)
        {
            return Task.FromResult(true);
        }

        public Task RegisterObjectAsync(IDBusObject o)
        {
            return Task.FromResult(true);
        }

        public Task RegisterObjectsAsync(IEnumerable<IDBusObject> objects)
        {
            return Task.FromResult(true);
        }

        public void UnregisterObject(ObjectPath path)
        {
            
        }

        public void UnregisterObject(IDBusObject dbusObject)
        {
        }

        public void UnregisterObjects(IEnumerable<ObjectPath> paths)
        {
        }

        public void UnregisterObjects(IEnumerable<IDBusObject> objects)
        {
        }

        #pragma warning disable 67
        public event EventHandler<ConnectionStateChangedEventArgs> StateChanged; 
    }
}