using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tmds.DBus;

namespace MellowPlayer.Connect.Lib.DBus
{
    public class PlayerProperties
    {
        public long Position = 0;
        public string PlaybackStatus = "None";
        public IDictionary<string, object> Metadata = new Dictionary<string, object>();
    }

    [DBusInterface("org.mpris.MediaPlayer2.Player")]
    public interface IPlayer : IDBusObject
    {
        Task PlayPauseAsync();
        Task NextAsync();
        Task PreviousAsync();

        Task<IDictionary<string, object>> GetAllAsync();
        Task<T> GetAsync<T>(string prop);
        Task SetAsync(string prop, object val);
        Task<IDisposable> WatchPropertiesAsync(Action<PropertyChanges> handler);
    }

    [DBusInterface("org.mpris.MediaPlayer2")]
    public interface IMediaPlayer : IPlayer
    {
        Task QuitAsync();
    }
}