namespace MellowPlayer.Connect.App.Hubs
{
    public class HubNotifiers
    {
        private readonly ApplicationHubNotifier _applicationHubNotifier;
        private readonly CurrentSongNotifier _currentSongNotifier;
        private readonly PlayerHubNotifier _playerHubNotifier;

        public HubNotifiers(ApplicationHubNotifier applicationHubNotifier, CurrentSongNotifier currentSongNotifier, PlayerHubNotifier playerHubNotifier)
        {
            // Keep notifiers alive
            _applicationHubNotifier = applicationHubNotifier;
            _currentSongNotifier = currentSongNotifier;
            _playerHubNotifier = playerHubNotifier;
        }
    }
}