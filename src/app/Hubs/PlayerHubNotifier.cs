using MellowPlayer.Connect.Lib.Player;
using Microsoft.AspNetCore.SignalR;

namespace MellowPlayer.Connect.App.Hubs
{
    public class PlayerHubNotifier
    {
        private readonly IHubContext<PlayerHub> _hubContext;
        private readonly IPlayer _player;

        public PlayerHubNotifier(IPlayer player, IHubContext<PlayerHub> hubContext)
        {
            _player = player;
            _hubContext = hubContext;

            _player.Status.PlayingChanged += (o, args) => _hubContext.Clients.All.SendAsync("PlayingChanged", _player.Status.IsPlaying);
            _player.Status.ServiceChanged += (o, args) => _hubContext.Clients.All.SendAsync("ServiceChanged", _player.Status.Service);
        }
    }
}