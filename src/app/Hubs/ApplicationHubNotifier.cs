using MellowPlayer.Connect.Lib.LocalApplication;
using Microsoft.AspNetCore.SignalR;

namespace MellowPlayer.Connect.App.Hubs
{
    public class ApplicationHubNotifier
    {
        private readonly ILocalApplication _application;
        private readonly IHubContext<ApplicationHub> _hubContext;

        public ApplicationHubNotifier(ILocalApplication application, IHubContext<ApplicationHub> hubContext)
        {
            _application = application;
            _hubContext = hubContext;

            _application.RunningChanged += (o, args) => _hubContext.Clients.All.SendAsync("RunningChanged", _application.IsRunning);
        }
    }
}