using MellowPlayer.Connect.Lib.Player;
using MellowPlayer.Connect.Lib.Song;
using Microsoft.AspNetCore.SignalR;

namespace MellowPlayer.Connect.App.Hubs
{
    public class CurrentSongHub : Hub
    {
        private readonly IPlayer _player;

        public CurrentSongHub(IPlayer player)
        {
            _player = player;
        }

        public ISong Data()
        {
            return _player.Status.CurrentSong;
        }

        public void ToggleFavorite()
        {
            _player.ToggleFavorite();
        }
    }
}