/**
 * The application hub allow to connect to the local MellowPlayer instance.
 *
 * It allows to:
 *  - interact with the local application (start)
 *  - get notified about any status change (running changed)
 */
class ApplicationHub extends Hub {
    constructor() {
        super("applicationHub");
        // Public properties
        this.running = undefined;
        this.canToggleFavorite = false;

        // Events
        this.runningChanged = new Event('applicationHub.runningChanged');
        this.canToggleFavoriteChanged = new Event('applicationHub.canToggleFavoriteChanged');
    }

    /**
     * Starts the local MellowPlayer application
     */
    start() {
        this._log("Starting");
        this._connection.invoke("Start").then(function () {
            this._log("started");
        }.bind(this));
    }

    _onRunningChanged(isRunning) {
        this._log("RunningChanged: " + isRunning);
        if (isRunning !== this.running) {
            this._log("runningChanged: " + isRunning);
            this.running = isRunning;
            window.dispatchEvent(this.runningChanged);
        }
    }

    _onCanToggleFavoriteChanged(canToggleFavorite) {
        this.canToggleFavorite = canToggleFavorite;
        window.dispatchEvent(this.canToggleFavoriteChanged);
    }

    onConnected() {
        super.onConnected();
        this._connection.on("RunningChanged", this._onRunningChanged.bind(this));
        this._connection.invoke("IsRunning").then(this._onRunningChanged.bind(this));
        this._connection.invoke("CanToggleFavorite").then(this._onCanToggleFavoriteChanged.bind(this));
    }

    onDisconnected() {
        super.onDisconnected();
        this.running = undefined;
    }
}