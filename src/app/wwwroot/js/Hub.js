class Hub {
    constructor(hubName) {
        this.connected = false;
        this.connectedChanged = new Event(hubName + '.connectedChanged');
        this._hubName = hubName;
        this._connectionString = "/" + hubName;
    }

    connect() {
        this._log("Connecting");
        this._connection = new signalR.HubConnectionBuilder().withUrl(this._connectionString).build();
        this._connection.start()
            .then(this.onConnected.bind(this))
            .catch(this.onDisconnected.bind(this));
        this._connection.onclose(this.onDisconnected.bind(this))
    }

    onConnected() {
        this._log("Connected");
        this.connected = true;
        window.dispatchEvent(this.connectedChanged);
    }

    onDisconnected() {
        this._log("Disconnected");
        this.connected = false;
        window.dispatchEvent(this.connectedChanged);
        setTimeout(this.connect.bind(this), 100);
    }

    _log(message) {
        console.log("[" + this._hubName + "] " + message);
    }
}