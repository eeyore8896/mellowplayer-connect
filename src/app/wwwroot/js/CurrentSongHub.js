/**
 * The application hub allow to connect to the local MellowPlayer instance.
 *
 * It allows to:
 *  - interact with the local application (start)
 *  - get notified about any status change (running changed)
 */
class CurrentSongHub extends Hub {
    constructor() {
        super("currentSongHub")

        // Public properties
        this.title = "";
        this.album = "";
        this.artist = "";
        this.artUrl = "";
        this.favorite = false;

        // Events
        this.changed = new Event('currentSongHub.changed');
        this.favoriteChanged = new Event('currentSongHub.favoriteChanged');
    }

    toggleFavorite() {
        this._log("Invoking ToggleFavorite");
        this._connection.invoke("ToggleFavorite").then(function () {
            this._log("ToggleFavorite invoked");
        }.bind(this));
    }

    isValid() {
        return this.title !== "" && this.title !== null;
    }

    onConnected() {
        super.onConnected();
        this._connection.on("Changed", this._onSongReceived.bind(this));
        this._connection.on("FavoriteChanged", this._onFavoriteChanged.bind(this));
        this._connection.invoke("Data").then(this._onSongReceived.bind(this));
    }

    _onSongReceived(song) {
        var equals = this.title === song.title;
        equals &= this.album === song.album;
        equals &= this.artist === song.artist;
        equals &= this.artUrl === song.artUrl;

        if (!equals) {
            this._log("Changed: " + JSON.stringify(song));
            this.title = song.title;
            this.artist = song.artist;
            this.album = song.album;
            this.artUrl = song.artUrl;
            window.dispatchEvent(this.changed);
        }

        this._onFavoriteChanged(song.isFavorite);
    }

    _onFavoriteChanged(isFavorite) {
        if (isFavorite !== this.favorite) {
            this.favorite = isFavorite;
            window.dispatchEvent(this.favoriteChanged);
        }
    }
}