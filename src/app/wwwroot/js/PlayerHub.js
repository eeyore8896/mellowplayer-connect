/**
 * The application hub allow to connect to the local MellowPlayer instance.
 *
 * It allows to:
 *  - interact with the local application (start)
 *  - get notified about any status change (running changed)
 */
class PlayerHub extends Hub {
    constructor() {
        super("playerHub");

        // Public properties
        this.playing = undefined;
        this.service = "";

        // Events
        this.playingChanged = new Event('playerHub.playingChanged');
        this.serviceChanged = new Event('playerHub.serviceChanged');
    }

    playPause() {
        this._log("Invoking PlayPause");
        this._connection.invoke("PlayPause").then(function () {
            this._log("PlayPause invoked");
        }.bind(this));
    }

    next() {
        this._log("Invoking Next");
        this._connection.invoke("Next").then(function () {
            this._log("Next invoked");
        }.bind(this));
    }

    previous() {
        this._log("Invoking Previous");
        this._connection.invoke("Previous").then(function () {
            this._log("Previous invoked");
        }.bind(this));
    }

    onConnected() {
        super.onConnected();
        this._connection.on("PlayingChanged", this._onPlayingChanged.bind(this));
        this._connection.on("ServiceChanged", this._onServiceChanged.bind(this));
        this._connection.invoke("IsPlaying").then(this._onPlayingChanged.bind(this));
        this._connection.invoke("Service").then(this._onServiceChanged.bind(this));
    }

    _onPlayingChanged(isPlaying) {
        if (this.playing !== isPlaying) {
            this._log("PlayingChanged: " + isPlaying);
            this.playing = isPlaying;
            window.dispatchEvent(this.playingChanged);
        }
    }

    _onServiceChanged(service) {
        if (this.service !== service) {
            this._log("ServiceChanged: " + service);
            this.service = service;
            window.dispatchEvent(this.serviceChanged);
        }
    }
}