﻿using System;
using System.Runtime.InteropServices;
using MellowPlayer.Connect.App.Hubs;
using MellowPlayer.Connect.Lib.DBus;
using MellowPlayer.Connect.Lib.LocalApplication;
using MellowPlayer.Connect.Lib.Player;
using MellowPlayer.Connect.Lib.Song;
using MellowPlayer.Connect.Lib.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using IPlayer = MellowPlayer.Connect.Lib.Player.IPlayer;
using IDbusConnectionFactory = MellowPlayer.Connect.Lib.DBus.IConnectionFactory;

namespace MellowPlayer.Connect.App
{
    public class Startup
    {
        private HubNotifiers _hubNotifiers;
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _hubNotifiers = null;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureDi(services);

            // Signal R Setup
            services.AddSignalR();

            // Basic Setup
            services.AddMvc(option => option.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        private static void ConfigureDi(IServiceCollection services)
        {
            services.AddTransient<IFileWatcher, FileWatcher>();
            services.AddTransient<ITimer, Timer>();

            services.AddSingleton<IDbusConnectionFactory, DbusSessionConnectionFactory>();
            services.AddSingleton<ILocalProcess, LocalProcess>();
            services.AddSingleton<ILocalApplication, LocalApplication>();
            services.AddSingleton<IPlayer, Player>();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                services.AddSingleton<ILocalProcessInteractionMethod, LocalProcessInteractionMethod>();
                services.AddSingleton<IInteractionMethod, MprisInteractionMethod>();
            }
            else
            {
                services.AddSingleton<IInteractionMethod, LocalProcessInteractionMethod>();
            }

            services.AddSingleton<IStatusFile, StatusFile>();

            services.AddSingleton<IProcessFactory, ProcessFactory>();
            services.AddSingleton<ITextFileFactory, TextFileFactory>();
            services.AddSingleton<ISongFactory, SongFactory>();
            services.AddSingleton<IStatusFactory, StatusFactory>();

            services.AddSingleton<ApplicationHubNotifier>();
            services.AddSingleton<PlayerHubNotifier>();
            services.AddSingleton<CurrentSongNotifier>();
            services.AddSingleton<HubNotifiers>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, HubNotifiers hubNotifiers, ILocalApplication localApplication)
        {
            if (!localApplication.IsAvailable)
                throw new SystemException("MellowPlayer Process Not found");
            
            _hubNotifiers = hubNotifiers; // keep hub notifiers alive during the whole application lifecycle
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ApplicationHub>("/applicationHub");
                endpoints.MapHub<PlayerHub>("/playerHub");
                endpoints.MapHub<CurrentSongHub>("/currentSongHub");
            });
        }
    }
}