using MellowPlayer.Connect.Lib.LocalApplication;
using MellowPlayer.Connect.Lib.LocalApplication.Fakes;
using MellowPlayer.Connect.Lib.Player;
using MellowPlayer.Connect.Lib.Player.Fakes;
using MellowPlayer.Connect.Lib.Song;
using MellowPlayer.Connect.Lib.Song.Fakes;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.Player
{
    public class StatusTests
    {
        private readonly FakeLocalProcess _localProcess = new FakeLocalProcess();
        private readonly FakeSongFactory _songFactory = new FakeSongFactory();
        private Status _sut;
        private bool _currentSongChanged;
        private bool _playingChanged;
        private bool _serviceChanged;
        
        [SetUp]
        public void SetUp()
        {
            _currentSongChanged = false;
            _playingChanged = false;
            _serviceChanged = false;
            
            _sut = new Status((ILocalProcess) _localProcess, (ISongFactory) _songFactory, NullLogger<Status>.Instance);
            _sut.CurrentSongChanged += (sender, args) => _currentSongChanged = true;
            _sut.PlayingChanged += (sender, args) => _playingChanged = true;
            _sut.ServiceChanged += (sender, args) => _serviceChanged = true;
        }

        [Test]
        public void DefaultProperties()
        {
            Assert.IsTrue(_sut.CurrentSong is NullSong); 
            Assert.IsFalse(_sut.IsPlaying);
            Assert.AreEqual("", _sut.Service);
        }

        [Test]
        public void Equals_SameStatus_True()
        {
            Assert.IsTrue(_sut.Equals(new Status((ILocalProcess) _localProcess, (ISongFactory) _songFactory, NullLogger<Status>.Instance)));
        }
        
        [Test]
        public void Equals_DifferentSong_False()
        {
            Assert.IsFalse(_sut.Equals(new FakeStatus(new FakeSong(), false, "")));
        }
        
        [Test]
        public void Equals_DifferentIsPlaying_False()
        {
            Assert.IsFalse(_sut.Equals(new FakeStatus(new NullSong(), true, "")));
        }
        
        [Test]
        public void Equals_DifferentService_False()
        {
            Assert.IsFalse(_sut.Equals(new FakeStatus(new NullSong(), false, "Deezer")));
        }

        [Test]
        public void Update_DifferentSongAndApplicationIsRunning_SongIsUpdated()
        {
            _localProcess.IsRunning = true;
            
            _sut.Update(new FakeStatus(new FakeSong(), false, ""));
            
            Assert.IsTrue(_sut.CurrentSong is FakeSong);
            Assert.IsTrue(_currentSongChanged);
        }
        
        [Test]
        public void Update_DifferentSongAndApplicationIsNotRunning_SongIsNotUpdated()
        {
            _localProcess.IsRunning = false;
            
            _sut.Update(new FakeStatus(new FakeSong(), false, ""));
            
            Assert.IsTrue(_sut.CurrentSong is NullSong);
            Assert.IsFalse(_currentSongChanged);
        }
        
        [Test]
        public void Update_DifferentIsPlayingAndApplicationIsRunning_IsPlayingIsUpdated()
        {
            _localProcess.IsRunning = true;
            
            _sut.Update(new FakeStatus(new NullSong(), true, ""));
            
            Assert.IsTrue(_sut.IsPlaying);
            Assert.IsTrue(_playingChanged);
        }
        
        [Test]
        public void Update_DifferentIsPlayingAndApplicationIsNotRunning_IsPlayingIsNotUpdated()
        {
            _localProcess.IsRunning = false;
            
            _sut.Update(new FakeStatus(new NullSong(), true, ""));
            
            Assert.IsFalse(_sut.IsPlaying);
            Assert.IsFalse(_playingChanged);
        }
        
        [Test]
        public void Update_DifferentServiceAndApplicationIsRunning_ServiceIsUpdated()
        {
            _localProcess.IsRunning = true;
            
            _sut.Update(new FakeStatus(new NullSong(), false, "Deezer"));
            
            Assert.AreEqual("Deezer", _sut.Service);
            Assert.IsTrue(_serviceChanged);
        }
        
        [Test]
        public void Update_DifferentServiceAndApplicationIsNotRunning_ServiceIsNotUpdated()
        {
            _localProcess.IsRunning = false;
            
            _sut.Update(new FakeStatus(new NullSong(), false, "Deezer"));
            
            Assert.AreEqual("", _sut.Service);
            Assert.IsFalse(_serviceChanged);
        }

        [Test]
        public void Deserialize_FromValidJsonString_CorrectProperties()
        {
            _localProcess.IsRunning = true;
            var jsonString = @"
{
    ""currentSong"": {
        ""album"": """",
        ""artUrl"": ""https://thumbnailer.mixcloud.com/unsafe/512x512/extaudio/9/8/a/f/03dc-0afe-4f86-a989-03619519462f"",
        ""artist"": ""Beats Behind The Sun"",
        ""isFavorite"": true,
        ""title"": ""Late Night High #2""
    },
    ""isPlaying"": true,
    ""serviceName"": ""Mixcloud""
}";
            _sut.Deserialize(jsonString);
            
            Assert.IsTrue(_sut.IsPlaying);
            Assert.AreEqual("Mixcloud", _sut.Service);
            Assert.AreEqual("Late Night High #2", _sut.CurrentSong.Title);
            Assert.AreEqual("Beats Behind The Sun", _sut.CurrentSong.Artist);
            Assert.AreEqual("", _sut.CurrentSong.Album);
            Assert.AreEqual("https://thumbnailer.mixcloud.com/unsafe/512x512/extaudio/9/8/a/f/03dc-0afe-4f86-a989-03619519462f", _sut.CurrentSong.ArtUrl);
            Assert.IsTrue(_sut.CurrentSong.IsFavorite);
        }
    }
}