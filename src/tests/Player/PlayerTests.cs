using System;
using MellowPlayer.Connect.Lib.LocalApplication.Fakes;
using MellowPlayer.Connect.Lib.Player;
using MellowPlayer.Connect.Lib.Player.Fakes;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.Player
{
    public class PlayerTests
    {
        private readonly FakeStatusFactory _statusFactory = new FakeStatusFactory();
        private FakeInteractionMethod _interactionMethod;
        private IPlayer _sut;
        private bool _currentSongChanged;

        [SetUp]
        public void SetUp()
        {
            _interactionMethod = new FakeInteractionMethod();
            _currentSongChanged = false;
            _sut = new Connect.Lib.Player.Player(_interactionMethod, _statusFactory, NullLogger<Connect.Lib.Player.Player>.Instance);
            _sut.Status.CurrentSongChanged += OnCurrentSongChanged;
        }

        private void OnCurrentSongChanged(object sender, EventArgs e)
        {
            _currentSongChanged = true;
        }

        [Test]
        public void PlayPause_Twice_IsPlayingChangedEachTime()
        {
            _sut.PlayPause();
            Assert.IsTrue(_sut.Status.IsPlaying);
            
            _sut.PlayPause();
            Assert.IsFalse(_sut.Status.IsPlaying);
        }
        
        [Test]
        public void Next_CurrentSongChanged()
        {
            var previousSong = _sut.Status.CurrentSong;
            
            _sut.Next();
            Assert.IsFalse(previousSong.Equals(_sut.Status.CurrentSong));
            Assert.IsTrue(_currentSongChanged);
        }
        
        [Test]
        public void Previous_CurrentSongChanged()
        {
            var previousSong = _sut.Status.CurrentSong;
            
            _sut.Previous();
            Assert.IsFalse(previousSong.Equals(_sut.Status.CurrentSong));
            Assert.IsTrue(_currentSongChanged);
        }
        
        [Test]
        public void ToggleFavorite_Twice_IsFavoriteBecomesFalseThenTrue()
        {
            Assert.IsFalse(_sut.Status.CurrentSong.IsFavorite);
            _sut.ToggleFavorite();
            
            Assert.IsTrue(_sut.Status.CurrentSong.IsFavorite);
            Assert.IsTrue(_currentSongChanged);

            _currentSongChanged = false;
            
            _sut.ToggleFavorite();
            Assert.IsFalse(_sut.Status.CurrentSong.IsFavorite);
            Assert.IsTrue(_currentSongChanged);
        }
    }
}