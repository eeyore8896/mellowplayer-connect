using System.Threading;
using NUnit.Framework;
using Timer = MellowPlayer.Connect.Lib.Utils.Timer;

namespace MellowPlayer.Connect.Tests.Utils
{
    public class TimerTests
    {
        private Timer _sut;
        private int _callbackCounter;
        
        [SetUp]
        public void SetUp()
        {
            _callbackCounter = 0;
            _sut = new Timer();
        }

        [TearDown]
        public void TearDown()
        {
            _sut.Stop();
        }

        private void IncrementCounter()
        {
            _callbackCounter++;
        }

        [Test, Retry(3)]
        public void Start_AutoReset_CallbackCalledRepeatedly()
        {
            _sut.Start(10, IncrementCounter);
            Thread.Sleep(100);
            Assert.Greater(_callbackCounter, 1);
        }

        [Test, Retry(3)]
        public void Stop_CallbackNotCalledAnyMore()
        {
            _sut.Start(10, IncrementCounter);
            Thread.Sleep(100);

            Assert.GreaterOrEqual(_callbackCounter, 1);
            var callbackCounter = _callbackCounter;
            _sut.Stop();
            
            Thread.Sleep(100);
            
            Assert.AreEqual(callbackCounter, _callbackCounter);
        }
        
        [Test, Retry(3)]
        public void Start_NoAutoReset_CallbackCalledOnce()
        {
            _sut.Start(10, false, IncrementCounter);
            Thread.Sleep(100);
            Assert.AreEqual(1, _callbackCounter);
        }
    }
}