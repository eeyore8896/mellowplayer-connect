using System;
using System.IO;
using MellowPlayer.Connect.Lib.Utils;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.Utils
{
    public class TextFileTests
    {
        private TextFile _sut;
        private string _filePath;
        
        private const string ExpectedContent = "Hello World!\n";
        private const string InvalidFilePath = "/invalid/file/path.txt";

        [SetUp]
        public void Setup()
        {
            _filePath = $"{System.IO.Path.GetTempPath()}{Guid.NewGuid()}.txt";
            _sut = new TextFile(_filePath);
           
           File.WriteAllText(_filePath, ExpectedContent);
        }

        [Test]
        public void Read_FileExist_ContentIsCorrectlyRetrieved()
        {
            Assert.IsTrue(_sut.Exists());
            Assert.AreEqual(ExpectedContent, _sut.Read());
        }
        
        [Test]
        public void Read_FileDoesNotExist_ContentIsEmpty()
        {
            var sut = new TextFile("/path/to/non/existing/file");
            Assert.IsFalse(sut.Exists());
            Assert.AreEqual("", sut.Read());
        }
    }
}