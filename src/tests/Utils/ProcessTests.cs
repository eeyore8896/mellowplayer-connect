using System.Threading;
using MellowPlayer.Connect.Lib.Utils;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.Utils
{
    public class ProcessTests
    {
        private Process _sut;
        private readonly ProcessFactory _processFactory = new ProcessFactory();

        [SetUp]
        public void SetUp()
        {
            _sut = new Process("ls", "-a", true);
        }

        [Test]
        public void IsRunning_CurrentProcess_True()
        {
            Assert.IsTrue(new Process("dotnet").IsRunning());
        }
        
        [Test]
        public void IsRunning_ProcessNotBeingRun_False()
        {
            Assert.IsFalse(new Process("cal").IsRunning());
        }

        [Test]
        public void Start_WaitForFinished_ExitCodeIsZero()
        {
            _sut.Start();
            _sut.WaitForExit();
            
            Assert.AreEqual(0,_sut.ExitCode);
        }
        
        [Test]
        public void Start_WaitForFinished_StandardOutputIsNotEmpty()
        {
            _sut.Start();
            _sut.WaitForExit();
            
            Assert.AreNotEqual("",_sut.StandardOutput);
        }
        
        [Test]
        public void Start_WaitForFinished_ErrorOutputIsEmpty()
        {
            _sut.Start();
            _sut.WaitForExit();
            
            Assert.AreEqual("",_sut.ErrorOutput);
        }

        [Test]
        public void Terminate_LongRunningCommand_ExitCodeIsNotZero()
        {
            var sut = _processFactory.Create("ping", "google.com");
            
            sut.Start();
            Thread.Sleep(100);
            sut.Terminate();
            
            Assert.AreNotEqual(0,_sut.ExitCode);
        }
    }
}