using MellowPlayer.Connect.Lib.Song;
using MellowPlayer.Connect.Lib.Song.Fakes;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.Song
{
    public class NullSongTests
    {
        private NullSong _sut;
        private bool _favoriteChanged;

        [SetUp]
        public void SetUp()
        {
            _sut = new NullSong();
            _sut.FavoriteChanged += (sender, args) => _favoriteChanged = true;
            _favoriteChanged = false;
        }

        [Test]
        public void PropertiesAreCorrectlyInitialized()
        {
            CheckDefaultProperties();
        }

        private void CheckDefaultProperties()
        {
            Assert.AreEqual("", _sut.Title);
            Assert.AreEqual("", _sut.Album);
            Assert.AreEqual("", _sut.Artist);
            Assert.AreEqual("", _sut.ArtUrl);
            Assert.IsFalse(_sut.IsFavorite);
        }

        [Test]
        public void ToggleFavoriteDoesNothing()
        {
            _sut.ToggleFavorite();
            
            Assert.IsFalse(_sut.IsFavorite);
        }

        [Test]
        public void Equals_OtherNullSong_True()
        {
            Assert.IsTrue(_sut.Equals(new NullSong()));
        }
        
        [Test]
        public void Equals_OtherSong_False()
        {
            Assert.IsFalse(_sut.Equals(new FakeSong()));
        }
        
        [Test]
        public void Update_RaiseIsFavoriteChanged()
        {
            _sut.Update(new FakeSong());
            
            Assert.IsTrue(_favoriteChanged);
        }
    }
}