using System.Collections.Generic;
using MellowPlayer.Connect.Lib.DBus.Fakes;
using MellowPlayer.Connect.Lib.LocalApplication;
using MellowPlayer.Connect.Lib.LocalApplication.Fakes;
using MellowPlayer.Connect.Lib.Player.Fakes;
using MellowPlayer.Connect.Lib.Song.Fakes;
using NUnit.Framework;
using Tmds.DBus;

namespace MellowPlayer.Connect.Tests.LocalApplication
{
    public class MprisInteractionMethodTests
    {
        private MprisInteractionMethod _sut;
        private readonly FakeStatusFactory _statusFactory = new FakeStatusFactory();
        private readonly FakeSongFactory _songFactory = new FakeSongFactory();
        private readonly FakeConnectionFactory _connectionFactory = new FakeConnectionFactory();
        private readonly FakeLocalProcessInteractionMethod _localProcessInteractionMethod = new FakeLocalProcessInteractionMethod();
        
        [SetUp]
        public void SetUp()
        {
            _sut = new MprisInteractionMethod(_statusFactory, _songFactory, _connectionFactory, _localProcessInteractionMethod);
        }

        [Test]
        public void InitialState_PropertiesAreSetToInitialValues()
        {
            Assert.AreEqual(MprisPlayer.InitialSongtitle, _sut.PlayerStatus.CurrentSong.Title);
            Assert.AreEqual(MprisPlayer.InitialAlbum, _sut.PlayerStatus.CurrentSong.Album);
            Assert.AreEqual(MprisPlayer.InitialArtist, _sut.PlayerStatus.CurrentSong.Artist);
            Assert.AreEqual(MprisPlayer.InitialArtUrl, _sut.PlayerStatus.CurrentSong.ArtUrl);
            Assert.AreEqual(MprisPlayer.InitialUserRating > 0, _sut.PlayerStatus.CurrentSong.IsFavorite);
            Assert.IsTrue(_sut.PlayerStatus.IsPlaying);
        }

        [Test]
        public void Execute_TogglePlayPause_CallPlayPauseOnFakePlayerProxy()
        {
            _sut.Execute(Action.TogglePlayPause);
            
            Assert.IsTrue(MprisPlayer.PlayPauseCalled);
        }

        private FakePlayer MprisPlayer => _connectionFactory.FakeConnection.Player;

        [Test]
        public void Execute_GoToNext_CallNextOnFakePlayerProxy()
        {
            _sut.Execute(Action.GoToNext);
            
            Assert.IsTrue(_connectionFactory.FakeConnection.Player.NextCalled);
        }
        
        [Test]
        public void Execute_GoToPrevious_CallPreviousOnFakePlayerProxy()
        {
            _sut.Execute(Action.GoToPrevious);
            
            Assert.IsTrue(_connectionFactory.FakeConnection.Player.PreviousCalled);
        }
        
        [Test]
        public void Execute_ToggleFavorite_CallExecuteOnLocalProcess()
        {
            _sut.Execute(Action.ToggleFavorite);
            
            Assert.IsTrue(_localProcessInteractionMethod.ExecutedActions.Contains(Action.ToggleFavorite));
        }
        
        [Test]
        public void Execute_PrintVersion_CallExecuteOnLocalProcess()
        {
            _sut.Execute(Action.PrintVersion);
            
            Assert.IsTrue(_localProcessInteractionMethod.ExecutedActions.Contains(Action.PrintVersion));
        }

        [Test]
        public void OnPropertiesChanged_PlaybackStatusIsPaused_IsPlayingIsFalse()
        {
            var metadata = new Dictionary<string, object>();
            metadata["origin"] = "MellowPlayer";
            var keyValuePairs = new KeyValuePair<string, object>[]
            {
                new KeyValuePair<string, object>("PlaybackStatus", "Paused"),
                new KeyValuePair<string, object>("Metadata", metadata),
            };
            var changes = new PropertyChanges(keyValuePairs);
            
            MprisPlayer.OnPropertiesChanged(changes);
            
            Assert.IsFalse(_sut.PlayerStatus.IsPlaying);
        }
        
        [Test]
        public void OnPropertiesChanged_MetadataChanged_CurrentSongPropertiesAreCorrect()
        {
            var metadata = new Dictionary<string, object>();
            metadata["xesam:title"] = $"{MprisPlayer.InitialSongtitle}2";
            metadata["xesam:artist"] = new string[]{$"{MprisPlayer.InitialArtist}2"};
            metadata["xesam:album"] = $"{MprisPlayer.InitialAlbum}2";
            metadata["xesam:userRating"] = 0;
            metadata["origin"] = "MellowPlayer";
            var keyValuePairs = new KeyValuePair<string, object>[]
            {
                new KeyValuePair<string, object>("Metadata", metadata),
            };
            var changes = new PropertyChanges(keyValuePairs);
            
            MprisPlayer.OnPropertiesChanged(changes);
            
            Assert.AreEqual($"{MprisPlayer.InitialSongtitle}2", _sut.PlayerStatus.CurrentSong.Title);
            Assert.AreEqual($"{MprisPlayer.InitialAlbum}2", _sut.PlayerStatus.CurrentSong.Album);
            Assert.AreEqual($"{MprisPlayer.InitialArtist}2", _sut.PlayerStatus.CurrentSong.Artist);
            Assert.IsFalse(_sut.PlayerStatus.CurrentSong.IsFavorite);
        }
    }
}