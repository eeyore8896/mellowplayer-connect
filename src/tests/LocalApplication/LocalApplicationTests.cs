using MellowPlayer.Connect.Lib.LocalApplication;
using MellowPlayer.Connect.Lib.LocalApplication.Fakes;
using MellowPlayer.Connect.Lib.Utils.Fakes;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.LocalApplication
{
    public class LocalApplicationTests
    {
        private readonly FakeLocalProcess _localProcess = new FakeLocalProcess();
        private readonly FakeTimer _timer = new FakeTimer();
        
        [SetUp]
        public void SetUp()
        {
            FakeLocalProcess.ActionOutput[Action.PrintVersion] = "SomeGarbage\nMellowPlayer3 3.5.4\nSomeOtherGarbage";
            _localProcess.Exists = true;
        }

        private ILocalApplication CreateSut()
        {
            return new Connect.Lib.LocalApplication.LocalApplication(_localProcess, _timer, NullLogger<Connect.Lib.LocalApplication.LocalApplication>.Instance);
        }

        [Test]
        public void IsAvailable_LocalProcessExists_IsTrue()
        {
            var sut = CreateSut();
            
            Assert.IsTrue(sut.IsAvailable);
        }
        
        [Test]
        public void IsAvailable_LocalProcessDoesNotExist_IsFalse()
        {
            _localProcess.Exists = false;
            var sut = CreateSut();
            
            Assert.IsFalse(sut.IsAvailable);
        }
        
        [Test]
        public void Version_LocalProcessExists_ValidVersionString()
        {
            var sut = CreateSut();
            
            Assert.AreEqual("3.5.4", sut.Version);
        }
        
        [Test]
        public void Version_LocalProcessDoesNotExist_IsEmpty()
        {
            _localProcess.Exists = false;
            var sut = CreateSut();
            
            Assert.AreEqual("", sut.Version);
        }

        [Test]
        public void IsRunning_LocalProcessExistsButIsNotRunning_IsFalse()
        {
            _localProcess.IsRunning = false;
            var sut = CreateSut();
            
            Assert.IsFalse(sut.IsRunning);
        }
        
        [Test]
        public void IsRunning_LocalProcessIsNotRunningThenStart_IsFalseAfterTimerElapsed()
        {
            _localProcess.IsRunning = false;
            var sut = CreateSut();
            Assert.IsFalse(sut.IsRunning);
            
            _localProcess.IsRunning = true;
            Assert.IsFalse(sut.IsRunning);
            
            _timer.Trigger();
            Assert.IsTrue(sut.IsRunning);
        }
        
       
        [Test]
        public void IsRunning_LocalProcessExistsAndIsRunning_IsTrue()
        {
            _localProcess.IsRunning = true;
            var sut = CreateSut();
            
            Assert.IsTrue(sut.IsRunning);
        }

        [Test]
        public void IsRunning_LocalProcessIsRunningThenStop_IsFalseAfterTimerElapsed()
        {
            _localProcess.IsRunning = true;
            var sut = CreateSut();
            Assert.IsTrue(sut.IsRunning);

            _localProcess.IsRunning = false;
            Assert.IsTrue(sut.IsRunning);

            _timer.Trigger();
            Assert.IsFalse(sut.IsRunning);
        }

        [Test]
        public void Start_ProcessWasNotRunning_LocalProcessIsNowRunning()
        {
            _localProcess.IsRunning = false;
            var sut = CreateSut();
            
            sut.Start();
            Assert.IsTrue(_localProcess.IsRunning);
        }
        
        [Test]
        public void Start_ProcessWasRunning_DoesNothing()
        {
            _localProcess.IsRunning = true;
            var sut = CreateSut();
            
            Assert.DoesNotThrow(sut.Start);
            Assert.IsTrue(_localProcess.IsRunning);
        }

        [Test]
        public void CanToggleFavorite_Version341_False()
        {
            FakeLocalProcess.ActionOutput[Action.PrintVersion] = "MellowPlayer3 3.4.1";
            var sut = CreateSut();
            
            Assert.IsFalse(sut.CanToggleFavorite);
        }
        
        [Test]
        public void CanToggleFavorite_Version360_True()
        {
            FakeLocalProcess.ActionOutput[Action.PrintVersion] = "MellowPlayer3 3.6.0";
            var sut = CreateSut();
            
            Assert.IsTrue(sut.CanToggleFavorite);
        }
    }
}