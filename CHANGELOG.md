# Change log

## [0.2.0](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.0) (2020-1-1)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.1.0...0.2.0)

**Implemented enhancements:**

- Ability to specify custom MellowPlayer executable [\#2](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/2)
- Windows support [\#1](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/1)

## [0.1.0](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.1.0) (2019-12-30)

Initial release with basic MVP.
